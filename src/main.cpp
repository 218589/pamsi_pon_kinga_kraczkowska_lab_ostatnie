#include <math.h>
#include <GL/glut.h>
#include <cassert>
#include <cstdlib>
#include <utility>
#include <vector>
#include <math.h>
#include "gra.h"
/*http://home.agh.edu.pl/~panasiuk/pl/index.php?p=graf/8/pix*/
/*POlITECHNIKA WARSZAWSKA
WYDZIAŁ MECHANICZNY ENERGETYKI I LOTNICTWA WPROWADZENIE
DO SZTUCZNEJ INTELIGENCJI
MEL
NS 586*/
/*http://sequoia.ict.pwr.wroc.pl/~witold/aiarr/2009_projekty/warcaby/*/
/*http://phabas.republika.pl/opengl/html/glut_7.html*/
/*POLITECHNIKA WARSZAWSKA
WYDZIAŁ MATEMATYKI I NAUK INFORMACYJNYCH
Analiza algorytmów
dla gier
dwuosobowych
Autor : Marcin Borkowski
Promotor : dr Jan Bródka */
using namespace std;

Gra gra;

void display()
{
  glClear(GL_COLOR_BUFFER_BIT);// czyszczenie zawartości bufora koloru
  for (int x = 0; x < 8; ++x)
    for (int y = 0; y < 8; ++y)
   {
      if ((x + y) % 2)
        glColor3f(0.2f, 0.2f, 0.1f);
      else
        glColor3f(1.0f, 1.0f, 1.0f);
     glBegin(GL_QUADS);//kwadrat
      glVertex2f(x * 480 / 8, y * 480 / 8);//wspolrzędne kolejnych 4 pkt
      glVertex2f((x + 1) * 480 / 8, y * 480 / 8);//480 rozmiar okna
     glVertex2f((x + 1) * 480 / 8, (y + 1) * 480 / 8);
      glVertex2f(x * 480 / 8, (y + 1) * 480 / 8);
      glEnd();
      switch (gra.cell(x, y))//pole o indeksie
      {
      case Gra::EMPTY:
        break;
     case Gra::BLACK:
        glColor3f(0.98f, 0.4f, 0.70f);
        glBegin(GL_POLYGON);
        for (int a = 0; a < 15; ++a)//zapelnia plansze czarnymi pionkami
        {
          float xx = 480 / 8 / 2 * cos(2 * M_PI * a / 15) + (x + 0.5f) * 480 / 8;
          float yy =  480 / 8 / 2 * sin(2 * M_PI * a / 15) + (y + 0.5f) * 480 / 8;
          glVertex2f(xx, yy);
          
        }
        glEnd();
        break;
      case Gra::WHITE:
        Pos selected = gra.zaznaczonePole();
        if (selected.first == x && selected.second == y)
          glColor3f(0.7f, 0.7f, 1.0f);
        else
          glColor3f(0.7f, 1.0f, 0.7f);
        glBegin(GL_POLYGON);
        for (int a = 0; a < 15; ++a) //zapelnia plansze bialymi pionkami
        {
          float xx = 480 / 8 / 2 * cos(2 * M_PI * a / 15) + (x + 0.5f) * 480 / 8;
          float yy =  480 / 8 / 2 * sin(2 * M_PI * a / 15) + (y + 0.5f) * 480 / 8;
          glVertex2f(xx, yy);
          
        }
        glEnd();
        break;
      }
    }

 glutSwapBuffers();
}

void mouse(int button, int state, int x, int y)
{
  if (state == GLUT_UP)
  {
    vector<Ruchy> ruchy;
    gra.getListaRuchow (Gra::_BIALE, ruchy);
    if (ruchy.empty())
      gra = Gra();
    const int xx = x / (480 / 8);
    const int yy = y / (480 / 8);
    if (gra.zaznaczonePole() == make_pair(-1, -1))
      gra.zaznaczPole(xx, yy);
    else
      gra.ruch(xx, yy);
    glutPostRedisplay();// odświeżenie okna
  }
}

int main(int argc, char **argv)
{ 
  glutInit(&argc, argv);//Inicjalizacja biblioteki GLUT
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);//określenie trybu graficznego,
  glutInitWindowSize(480, 480); 
  glutCreateWindow("WARCABY");
  glutMouseFunc(mouse);//Rejestruje zdarzenia myszy
  glOrtho(0, 480, 480, 0, -1.0, 1.0);
  glutDisplayFunc(display);//odświeżenie zawartości okna generowanowanie sceny
  glutMainLoop();
}
