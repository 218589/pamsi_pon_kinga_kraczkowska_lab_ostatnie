
#include <math.h>
#include <GL/glut.h>
#include <cassert>
#include <cstdlib>
#include <utility>
#include <vector>
#include<iostream>
#include "gra.h"

using namespace std;

Gra::Gra()://konstruktor
  zaznaczonePoleX_(-1),
  zaznaczonePoleY_(-1),
  zbity(false)//na sam poczatek
{
  for (int x = 0; x < 8; ++x)
    for (int y = 0; y < 8; ++y)
      plansza[x][y] = EMPTY;//staworzeniec calej pustej planaszy
  for (int y = 0; y < 3; ++y)
    for (int x = (y + 1) % 2; x < 8; x += 2)
      plansza[x][y] = BLACK;
  for (int y = 5; y < 8; ++y)// 3 rzędy
    for (int x = (y + 1) % 2; x < 8; x += 2)//co drugie pole
      plansza[x][y] = WHITE;
                               
}

Gra::Pole Gra::cell(int x, int y) const
{
  return plansza[x][y];//zwraca pole o indeksie xy
}

Pos Gra::zaznaczonePole() const
{
  return make_pair(zaznaczonePoleX_, zaznaczonePoleY_);//kilikniecie myszka
}

void Gra::zaznaczPole(int x, int y)
{
  if (x < 8 && x >= 0 && y >= 0 && y < 8 && plansza[x][y] == WHITE)
  {
    zaznaczonePoleX_ = x;
    zaznaczonePoleY_ = y;
  }
  else//jesli puste lub przeciwnika
  {
    zaznaczonePoleX_ = -1;
    zaznaczonePoleY_ = -1;
  }
}

void Gra::ruch(int x, int y)
{
  assert(zaznaczonePoleX_ != -1); //jesli pole gracza
  assert(zaznaczonePoleY_ != -1);

  vector<Ruchy> listaMozliwRuchow;
  getListaRuchow (_BIALE, listaMozliwRuchow);
  if (listaMozliwRuchow.empty())//jesli nie ma ruchu to resetuj plansze
  {
    *this = Gra();//stworz plansze
    return;
  }

  Step step = Step(Pos(zaznaczonePoleX_, zaznaczonePoleY_), Pos(x, y));//klik na klki
  cout<<"Przesunięto z ("<<zaznaczonePoleX_<<","<<zaznaczonePoleY_<<") na ("<<x<<","<<y<<") "<<endl;
  bool przesuniety= false;
  

  for (vector<Ruchy>::iterator i = listaMozliwRuchow.begin(); i != listaMozliwRuchow.end(); ++i)
  {
    if (step == i->front() && (!zbity || (zbity && abs(zaznaczonePoleX_ - x) == 2)))
    {
      przesuniety= true;
      plansza[zaznaczonePoleX_][zaznaczonePoleY_] = EMPTY;
      plansza[x][y] = WHITE;
      if (abs(zaznaczonePoleX_ - x) == 2)
      {
        plansza[(zaznaczonePoleX_ + x) / 2][(zaznaczonePoleY_ + y) / 2] = EMPTY;
        zaznaczonePoleX_ = x;
        zaznaczonePoleY_ = y;
        zbity = true;
        return;
      }
      else
        zbity = false;
      break;
    }
  }
  
  if (zbity || przesuniety)
  {
    zbity = false;
    czarneRuchy();
  }
  zaznaczonePoleX_ = -1;
  zaznaczonePoleY_ = -1;
}

void Gra::getListaRuchow (Strona kolor, vector<Ruchy> &lolm) const
{
// Sprawdzenie bicia 
  for (int x = 0; x < 8; ++x)
    for (int y = 0; y < 8; ++y) //przeszukanie tablicy w celu znalezienia miejsc z pionkamai
      if (plansza[x][y] == static_cast<Pole>(kolor))
      {
        Ruchy ruch;
        znajdzBicia(x, y, kolor, ruch, lolm);// bicie jest obowiazkowe
      }
  
  if (lolm.empty())//szukanie mozliwych ruchow
  {
    // sprawdzanie ruchu
    for (int x = 0; x < 8; ++x)
      for (int y = 0; y < 8; ++y)//iteruj po planszy
        if (plansza[x][y] == static_cast<Pole>(kolor))//jesli odpowiedzni kolor
        {
          int yy = y + (kolor == _BIALE ? -1 : 1);//jesli kolor gracza to -1
          if (yy >= 0 && yy < 8)
            for (int xx = x - 1; xx <= x + 1; xx += 2)
            {
              if (xx < 0)
                continue;//to wyjdz z petli
              if (xx >= 8)
                continue;//to tez wez wyjdz
              if (plansza[xx][yy] == EMPTY)
              {
                Ruchy ruch;
                ruch.push_back(Step(Pos(x, y), Pos(xx, yy)));
                lolm.push_back(ruch);
              }
            }
        }
  }
}

void Gra::znajdzBicia(int x, int y, Strona kolor, Ruchy ruch, vector<Ruchy> &lolm) const
{/**/
  const int dx[] = {-1, 1, 1, -1}; //pary wpoolrzednych xy w 4 kierunkach
  const int dy[] = {-1, -1, 1, 1};//bo bicie we szystkie strony
  for (int d = 0; d < 4; ++d)
  {//powtorz w kazdym kierunku
    const int xx = x + dx[d];//co drugie pole
    const int yy = y + dy[d];
    if (xx < 0 || xx >= 8 || yy < 0 || yy >= 8)//dopoki na planszy
      continue;
    if (plansza[xx][yy] == EMPTY || plansza[xx][yy] == static_cast<Pole>(kolor))
      continue;//jesli kolor przeciwnika
    const int xxx = x + 2 * dx[d];//nastepny wiersz
    const int yyy = y + 2 * dy[d];
    if (xxx < 0 || xxx >= 8 || yyy < 0 || yyy >= 8)//dpokoi na planszy
      continue;
    if (plansza[xxx][yyy] == EMPTY)//najepier oklor przeciwniki potem pusty
    {					//mozliwe bicie
      bool samePlace = false;
      for (Ruchy::iterator i = ruch.begin(); i != ruch.end(); ++i)
        if (i->second == Pos(xxx, yyy))
        {
          samePlace = true;//wrocil
          break;//przerwij 
        }
      if (!samePlace)
      {//zbij i szukaj kolejnego
        ruch.push_back(Step(Pos(x, y), Pos(xxx, yyy)));
        lolm.push_back(ruch);
        znajdzBicia(xxx, yyy, kolor, ruch, lolm);
      }
    }
  }
 
 
  }

bool Gra::czarneRuchy(int level)
{
  vector<Ruchy> listaMozliwRuchow;
  getListaRuchow (_CZARNE, listaMozliwRuchow);
  if (listaMozliwRuchow.empty())
  {
    *this = Gra();
    return false;
  }
  int maxWynik = -30;
  vector<Ruchy>::iterator bestRuchy = listaMozliwRuchow.end();
  for (vector<Ruchy>::iterator ruch = listaMozliwRuchow.begin(); ruch != listaMozliwRuchow.end(); ++ruch)
  {
    Gra gra = *this;
    for (Ruchy::iterator i = ruch->begin(); i != ruch->end(); ++i)
    {
      gra.plansza[i->first.first][i->first.second] = EMPTY;
      gra.plansza[i->second.first][i->second.second] = BLACK;
      if (abs(i->second.first - i->first.first) == 2)
        gra.plansza[(i->first.first + i->second.first) / 2][(i->first.second + i->second.second) / 2] = EMPTY;
    }
    if (level == 0 || gra.bialeRuchy(level - 1))
    {
      int tmpWynik = gra.wynik();
      if (maxWynik < tmpWynik)
      {
        maxWynik = tmpWynik;
        bestRuchy = ruch;
      }
    } 
    else
    {
      maxWynik = 100;
      bestRuchy = ruch;
    }

  }
  for (Ruchy::iterator i = bestRuchy->begin(); i != bestRuchy->end(); ++i)
  {
    plansza[i->first.first][i->first.second] = EMPTY;
    plansza[i->second.first][i->second.second] = BLACK;
    if (abs(i->second.first - i->first.first) == 2)
      plansza[(i->first.first + i->second.first) / 2][(i->first.second + i->second.second) / 2] = EMPTY;
  }
  return true;
}

/*MINI-MAX(sytuacja, gracz, głębokość)
   if głębokość > limit_głębokości then
       return OCEŃ_SYTUACJĘ(sytuacja)
   r = WYGENERUJ_MOŻLIWE_RUCHY(sytuacja, gracz)
   if gracz = 0
       minimax := 0
   else
        minimax := inf
   foreach x in r
     sytuacja' := WYKONAJ_RUCH(sytuacja, x)
    t := MINI-MAX(sytuacja, PRZECIWNIK(gracz), głębokość + 1)
    if gracz = 0 and t > minimax then
     minimax:=t
    else if gracz = 1 and t < minimax then
     minimax:=t
   return minimax*/

bool Gra::bialeRuchy(int level)
{
  vector<Ruchy> listaMozliwRuchow;
  getListaRuchow (_BIALE, listaMozliwRuchow);
  if (listaMozliwRuchow.empty())
  {
    *this = Gra();
    return false;
  }
  int maxWynik = 30;
  vector<Ruchy>::iterator bestRuchy = listaMozliwRuchow.end();
  for (vector<Ruchy>::iterator ruch = listaMozliwRuchow.begin(); ruch != listaMozliwRuchow.end(); ++ruch)
  {
    Gra gra = *this;
    for (Ruchy::iterator i = ruch->begin(); i != ruch->end(); ++i)
    {
      gra.plansza[i->first.first][i->first.second] = EMPTY;
      gra.plansza[i->second.first][i->second.second] = WHITE;
      if (abs(i->second.first - i->first.first) == 2)
        gra.plansza[(i->first.first + i->second.first) / 2][(i->first.second + i->second.second) / 2] = EMPTY;
    }
    if (level == 0 || gra.czarneRuchy(level - 1))
    {
      int tmpWynik = gra.wynik();
      if (maxWynik > tmpWynik)
      {
        maxWynik = tmpWynik;
        bestRuchy = ruch;
      }
    } 
    else
    {
      maxWynik = -100;
      bestRuchy = ruch;
    }
  }
  for (Ruchy::iterator i = bestRuchy->begin(); i != bestRuchy->end(); ++i)
  {
    plansza[i->first.first][i->first.second] = EMPTY;
    plansza[i->second.first][i->second.second] = WHITE;
    if (abs(i->second.first - i->first.first) == 2)
      plansza[(i->first.first + i->second.first) / 2][(i->first.second + i->second.second) / 2] = EMPTY;
  }
  return true;
}

int Gra::wynik() const
{
  int res = 0;
  for (int x = 0; x < 8; ++x)
    for (int y = 0; y < 8; ++y)
      if (plansza[x][y] == BLACK)
        ++res;
      else if (plansza[x][y] == WHITE)
        --res;
  return res;
}



