#ifndef GRA_H
#define GRA_H

using namespace std;

typedef pair<int, int> Pos;
typedef pair<Pos, Pos> Step;
typedef vector<Step> Ruchy;


class Gra
{
public:
  enum Strona { _CZARNE = 1, _BIALE = 2 };
  enum Pole { EMPTY, BLACK = _CZARNE, WHITE = _BIALE };
  Gra();
  Pole cell(int x, int y) const;
  Pos zaznaczonePole() const; 
  void zaznaczPole(int x, int y);
  void ruch(int x, int y);
  void getListaRuchow (Strona, vector<Ruchy> &) const;
private:
  typedef Pole Tablica[8][8];
  Tablica plansza;
  int zaznaczonePoleX_;
  int zaznaczonePoleY_;
  bool zbity;
  void znajdzBicia(int x, int y, Strona kolor, Ruchy ruch, vector<Ruchy> &listaMozliwRuchow) const;
  bool czarneRuchy(int level = 1);
  bool bialeRuchy(int level = 1);
  int wynik() const;
};



#endif
