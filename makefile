NAME = ./bin/Lista78

OBJECTS = *.o
SOURCE = *.cpp
INCLUDES = *.h

BINDIR = bin
OBJDIR = obj
SRCDIR = src
INCDIR = inc

OBJECTS := ./$(OBJDIR)/$(OBJECTS)
SOURCE := ./$(SRCDIR)/$(SOURCE)
INCLUDES := ./$(INCDIR)/$(INCLUDES)

CPP = g++
CPPFLAGS = -pedantic -Wall -std=c++11 -I inc -g 2>&1   

LINKER = g++
LFLAGS =-lglut -lGL

$(NAME): $(OBJECTS)
	$(LINKER) $(OBJECTS) $(LFLAGS) -o $@ 

$(OBJECTS): $(SOURCE) $(INCLUDES)
	$(CPP) -c $(SOURCE) $(CPPFLAGS) 
	mv *.o $(OBJDIR)

clean:
	rm -f $(OBJECTS)
.PHONY: clean
